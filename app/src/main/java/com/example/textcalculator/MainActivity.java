package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    int firstOperand;
    int secondOperand;
    int result;
    EditText firstValue;
    EditText secondValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstValue = findViewById(R.id.firstOperand);
        secondValue = findViewById(R.id.secondOperand);
    }

    public void onClick(View view) {
        firstOperand = Integer.parseInt(firstValue.getText().toString());
        secondOperand = Integer.parseInt(secondValue.getText().toString());
        result = firstOperand + secondOperand;
        putIntent();
    }

    private void putIntent() {
        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra("first", firstOperand);
        intent.putExtra("second", secondOperand);
        intent.putExtra("result", result);
        startActivity(intent);
    }
}

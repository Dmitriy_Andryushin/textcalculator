package com.example.textcalculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ViewActivity extends Activity {

    TextView firstOperand;
    TextView secondOperand;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        firstOperand = findViewById(R.id.firstOperand);
        secondOperand = findViewById(R.id.secondOperand);
        result = findViewById(R.id.result);
        Intent intent = getIntent();
        int first = intent.getIntExtra("first",0);
        int second = intent.getIntExtra("second",0);
        int res = intent.getIntExtra("result",0);
        firstOperand.setText(String.valueOf(first));
        secondOperand.setText(String.valueOf(second));
        result.setText(String.valueOf(res));
    }
}